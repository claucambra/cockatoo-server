package platform

type PlatformConnectionStatus int

const (
	SignedOut PlatformConnectionStatus = iota + 1
	Disconnected
	Connected
	ServiceUnavailable
	NetworkError
	AskingCredentials
)

type Platform interface {
	// Methods to query platform state
	IsInConfig() (bool, error)
	Status() PlatformConnectionStatus

	// Methods to configure platform
	Username() string
	SetUsername(username string)
	Password() string
	SetPassword(password string)

	// Methods to perform actions in platform
	UploadPost() error
}
