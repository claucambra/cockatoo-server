package main

import (
	"cockatoo-server/pkg/plugin"
)

type MastodonPlugin struct {
	plugin.PlatformPlugin
	baseApiUrl string
}

func (mastoplugin MastodonPlugin) BaseApiUrl() string {
	return mastoplugin.baseApiUrl
}

func (mastoplugin *MastodonPlugin) SetBaseApiUrl(baseApiUrl string) {
	mastoplugin.baseApiUrl = baseApiUrl
}

func (mastoplugin MastodonPlugin) UploadPost(text string) {
	//log.Default("Starting Toot upload on Mastodon...")
}
