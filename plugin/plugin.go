package plugin

import (
	"strconv"
)

type PluginVersion struct {
	Major, Minor, Micro int
}

func (version PluginVersion) DisplayVersion() string {
	return strconv.Itoa(version.Major) + "." + strconv.Itoa(version.Minor) + "." + strconv.Itoa(version.Minor)
}

type Plugin interface {
	PluginName() string
	Version() PluginVersion
}
