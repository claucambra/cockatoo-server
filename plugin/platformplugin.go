package plugin

import (
	"errors"

	"github.com/spf13/viper"

	platform "cockatoo-server/pkg/platform"
)

type PlatformPlugin struct {
	Plugin
	platform.Platform
}

func (platform PlatformPlugin) IsInConfig() (bool, error) {
	if len(platform.PluginName()) == 0 {
		return false, errors.New("Plugin has no plugin name set")
	}

	return viper.IsSet(platform.PluginName()), nil
}
